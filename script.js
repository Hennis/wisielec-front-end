var WordList = ["pies", "kot", "krowa", "makaron", "poduszka", "komputer", "kubek", "drukarka", "poduszka", "modliszka", "myszka", "łodyga", "telefon", "dżem", "róza", "pizza", "żelki", "tchórz", "pomarańcza", "długopis"];
var text = WordList[Math.floor(Math.random() * WordList.length)].toUpperCase();
var wordArray = Array.from(text);
var invisible = [];
var usage = [];
var usageIndex = 0;
var fit = false;
var win=true;

function autorun()
    {
        document.getElementById("word").innerHTML = invisible.join(" ");
    }

window.onload = autorun;

for (var i=0; i<wordArray.length; i++)
    {
        invisible[i]="_";
    }


function clickKey(value) {

    document.getElementById(value).disabled = true;
    win=true;
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = "#FF0000";
    ctx.file
    ctx.beginPath();


    for(var i=0; i<wordArray.length; i++)
        {
            if(wordArray[i]==value)
                {
                    invisible[i]=value;
                    fit = true;
                }
            }

    if(fit==false)
        {
            usage[usageIndex]=value;
            usageIndex++;
        }

    for (var i=0; i<wordArray.length; i++)
        {
            if(wordArray[i]!=invisible[i])
                {
                    win=false;
                }
        }

    document.getElementById("word").innerHTML = invisible.join(" ");
    document.getElementById("uzyte").innerHTML = usage.join(" ");

    if(usageIndex==1)
        {
            ctx.moveTo(50, 230);
            ctx.lineTo(150, 230);
            ctx.stroke();
        }

    if(usageIndex==2)
        {
            ctx.moveTo(100, 230);
            ctx.lineTo(100, 30);
            ctx.stroke();
        }

    if(usageIndex==3)
        {
            ctx.moveTo(200, 30);
            ctx.lineTo(100, 30);
            ctx.stroke();
        }

    if(usageIndex==4)
        {
            ctx.moveTo(100, 60);
            ctx.lineTo(120, 30);
            ctx.stroke();
        }

    if(usageIndex==5)
        {
            ctx.moveTo(200, 55);
            ctx.lineTo(200, 30);
            ctx.stroke();
        }

    if(usageIndex==6)
        {
           ctx.arc(200, 70, 15, 0, 2 * Math.PI);
           ctx.stroke();
        }

    if(usageIndex==7)
        {
            ctx.moveTo(200, 140);
            ctx.lineTo(200, 85);
            ctx.stroke();
        }

    if(usageIndex==8)
        {
            ctx.moveTo(200, 140);
            ctx.lineTo(170, 180);
            ctx.stroke();
        }

    if(usageIndex==9)
        {
            ctx.moveTo(200, 140);
            ctx.lineTo(230, 180);
            ctx.stroke();
        }

    if(usageIndex==10)
        {
            ctx.moveTo(200, 110);
            ctx.lineTo(170, 95);
            ctx.stroke();
        }

    if(usageIndex==11)
        {
            ctx.moveTo(200, 110);
            ctx.lineTo(230, 95);
            ctx.stroke();
        }

    if(win==false && usageIndex==11)
        {
            if(confirm("PRZEGRAŁEŚ! \n Spróbuj jeszcze raz."))
                {
                    location.reload();
                }
         }

    if(win==true)
        {
            if(confirm("WYGRAŁEŚ! \n Zagraj jeszcze raz."))
                {
                    location.reload();
                }
        }

    fit=false;

    }

function newWord()
    {
        location.reload();
    }